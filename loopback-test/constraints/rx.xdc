# This is a constraints file for the rx to the transceiver.
# Clock declaration.
create_clock -period 15.625 -name rx_clk_ddr -waveform {0.000 7.813} [get_ports rx_clk_p]

# Set standard of ports.
set_property -dict {PACKAGE_PIN K18 IOSTANDARD LVDS_25} [get_ports rx_clk_n]
set_property -dict {PACKAGE_PIN K17 IOSTANDARD LVDS_25} [get_ports rx_clk_p]
set_property -dict {PACKAGE_PIN J20 IOSTANDARD LVDS_25} [get_ports rx_data_p]
set_property -dict {PACKAGE_PIN H20 IOSTANDARD LVDS_25} [get_ports rx_data_n]

# There are resistors on the board. No need for internal ones.
set_property DIFF_TERM FALSE [get_ports rx_clk_n]
set_property DIFF_TERM FALSE [get_ports rx_clk_p]
set_property DIFF_TERM FALSE [get_ports {rx_data_n[0]}]
set_property DIFF_TERM FALSE [get_ports {rx_data_p[0]}]

# Define skew_drv variable (0x0 = 1.906ns, 0x1 = 2.906ns, 0x2 = 3.906ns, 0x3 = 4.906ns)
set skew_drv 0x2

# Calculate delays based on skew_drv (min, typ, max)
if { $skew_drv == 0x0 } {
    set max_delay 3.1
    set typ_delay 1.9
    set min_delay 0.5
} elseif { $skew_drv == 0x1 } {
    set max_delay 3.9
    set typ_delay 2.9
    set min_delay 1.9
} elseif { $skew_drv == 0x2 } {
    set max_delay 4.9
    set typ_delay 3.9
    set min_delay 2.9
} elseif { $skew_drv == 0x3 } {
    set max_delay 5.9
    set typ_delay 4.9
    set min_delay 3.9
} else {
    error "Invalid SKEWDRV value. Must be 0x0, 0x1, 0x2, or 0x3."
}

# Set the jitter and skew constraints between clock and data as per the datasheet 10.15.
set_input_delay -clock [get_clocks rx_clk_ddr] -max $max_delay [get_ports rx_data_p]
set_input_delay -clock [get_clocks rx_clk_ddr] -clock_fall -max -add_delay $max_delay [get_ports rx_data_p]
set_input_delay -clock [get_clocks rx_clk_ddr] $typ_delay [get_ports rx_data_p]
set_input_delay -clock [get_clocks rx_clk_ddr] -clock_fall -add_delay $typ_delay [get_ports rx_data_p]
set_input_delay -clock [get_clocks rx_clk_ddr] -clock_fall -min -add_delay $min_delay [get_ports rx_data_p]
set_input_delay -clock [get_clocks rx_clk_ddr] -min $min_delay [get_ports rx_data_p]
set_clock_latency -source 0.492 [get_clocks rx_clk_ddr]
set_input_jitter [get_clocks rx_clk_ddr] 0.1


set_operating_conditions -grade extended
