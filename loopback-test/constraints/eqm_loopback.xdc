# General constraints for EQM.
# Set clock.
create_clock -period 10.000 -name sysclk -waveform {0.000 5.000} [get_ports sysclk]
set_property -dict {PACKAGE_PIN Y7 IOSTANDARD LVCMOS33} [get_ports sysclk]

# Clocking wizard requires the pk-pk period jitter. Oscillator datasheet declares
# period jitter as 5ps, RMS (at 125Mhz). In pk-pk terms it is 73.45ps for
# crest_factor = 14.069 (depends on the BER you select).
set_input_jitter [get_clocks sysclk] 0.073

# Delay caused by the trace length where propagation speed speed is
# approximately 150 mm/ps, with a 10% margin of error.
set_clock_latency -source -early 0.027 [get_clocks sysclk]
set_clock_latency -source -late 0.033 [get_clocks sysclk]
set_load -pin_load 15 [get_ports sysclk]


# Led port standard.
set_property -dict {PACKAGE_PIN N17 IOSTANDARD LVCMOS33} [get_ports led]










