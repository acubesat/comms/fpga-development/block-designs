# Constraints file for the SPI signals.
# Set SPI clock.
create_clock -period 40.000 -name SPI_CLK -waveform {0.000 20.000} [get_ports SPI_CLK]

# Set standard for ports.
set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets SPI_CLK_IBUF]
set_property -dict {PACKAGE_PIN Y17 IOSTANDARD LVCMOS33} [get_ports SPI_MISO]
set_property -dict {PACKAGE_PIN Y19 IOSTANDARD LVCMOS33} [get_ports SPI_MOSI]
set_property -dict {PACKAGE_PIN P20 IOSTANDARD LVCMOS33} [get_ports SPI_CLK]
set_property -dict {PACKAGE_PIN Y16 IOSTANDARD LVCMOS33} [get_ports SPI_SEL]

