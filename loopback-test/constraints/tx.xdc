# This is a constraints file for the tx to the transceiver.
# Clock declaration.
create_clock -period 15.625 -name tx_clk_ddr -waveform {0.000 7.813} [get_ports tx_clk_p]

# Set standard of ports.
set_property -dict {PACKAGE_PIN B20 IOSTANDARD LVDS_25} [get_ports tx_data_n]
set_property -dict {PACKAGE_PIN C20 IOSTANDARD LVDS_25} [get_ports tx_data_p]
set_property -dict {PACKAGE_PIN L17 IOSTANDARD LVDS_25} [get_ports tx_clk_n]
set_property -dict {PACKAGE_PIN L16 IOSTANDARD LVDS_25} [get_ports tx_clk_p]

# There are resistors on the board. No need for internal ones.
set_property DIFF_TERM FALSE [get_ports {tx_data_n[0]}]
set_property DIFF_TERM FALSE [get_ports {tx_data_p[0]}]
set_property DIFF_TERM FALSE [get_ports tx_clk_n]
set_property DIFF_TERM FALSE [get_ports tx_clk_p]

# Set the skew constraints between clock and data as per the datasheet 10.16.
set_output_delay -clock tx_clk_ddr -max 2.000 [get_ports tx_data_p]
set_output_delay -clock tx_clk_ddr -clock_fall -max -add_delay 2.000 [get_ports tx_data_p]
set_output_delay -clock tx_clk_ddr -min -1.000 [get_ports tx_data_p]
set_output_delay -clock tx_clk_ddr -clock_fall -min -add_delay -1.000 [get_ports tx_data_p]
set_clock_latency -source 0.542 [get_clocks tx_clk_ddr]

