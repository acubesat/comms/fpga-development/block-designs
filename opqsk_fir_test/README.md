This block design is created to help in testing different coefficient sets for the fir compilers of the OPQSK. It has one DMA to transfer to the oqpsk, one for the `CONFIG` channel and one for the `RELOAD` channel that the fir compiler needs to redefine the coefficients. The coefficient structure in the compiler is set as symmetric and thus only requires `roundup(num_of_coeffs / 2)` number of writes through the AXIS interface.  
**Custom IP Blocks**
- Verilog Oqpsk v2.2
- packetization v1.2
