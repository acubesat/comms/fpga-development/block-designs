#include "dma_simple_driver.h"
#include <xil_types.h>
#include <xstatus.h>

static int SetupDma(XAxiDma_Config *CfgPtr, XAxiDma *AxiDma, UINTPTR BaseAddress)
{
    int Status;
	CfgPtr = XAxiDma_LookupConfig(BaseAddress);
	if (!CfgPtr) {
		xil_printf("No config found for %d\r\n", BaseAddress);
		return XST_FAILURE;
	}

	Status = XAxiDma_CfgInitialize(AxiDma, CfgPtr);
	if (Status != XST_SUCCESS) {
		xil_printf("Initialization failed %d\r\n", Status);
		return XST_FAILURE;
	}

	if (XAxiDma_HasSg(AxiDma)) {
		xil_printf("Device configured as SG mode \r\n");
		return XST_FAILURE;
	}

	XAxiDma_IntrDisable(AxiDma, XAXIDMA_IRQ_ALL_MASK,
			    XAXIDMA_DEVICE_TO_DMA);
	XAxiDma_IntrDisable(AxiDma, XAXIDMA_IRQ_ALL_MASK,
			    XAXIDMA_DMA_TO_DEVICE);

    return XST_SUCCESS;
}

static void WriteReadDataLoop(XAxiDma *AxiDma, UINTPTR TxBufferPtr, UINTPTR RxBufferPtr, uint32_t PacketsIn, uint32_t PacketsOut)
{
	int Status = XST_SUCCESS;

    /* 
     * Send data from peripheral to DMA after checking whether the DMA has a
     * slave to receive data.
     */
    if (AxiDma-> HasS2Mm) {
	    Xil_DCacheFlushRange( RxBufferPtr, PacketsOut);
        Status = XAxiDma_SimpleTransfer(AxiDma, RxBufferPtr,
            PacketsOut, XAXIDMA_DEVICE_TO_DMA);
    } 

    if (Status != XST_SUCCESS) {
        xil_printf("Simple transfer from device to DMA failed");
        return;
    }
    
    /* 
     * Send data from DMA to peripheral after checking whether the DMA has a
     * master to send data.
     */
    Status = XST_SUCCESS;
	if (AxiDma->HasMm2S) {
        Xil_DCacheFlushRange( TxBufferPtr, PacketsIn);
        Status = XAxiDma_SimpleTransfer(AxiDma, TxBufferPtr,
            PacketsIn, XAXIDMA_DMA_TO_DEVICE);
    }

    if (Status != XST_SUCCESS) {
        xil_printf("Simple transfer from DMA to device failed");
        return;
    }

    /* Wait for the respective processes to finish only if they are existant */
    if (AxiDma-> HasS2Mm) {
        while ( XAxiDma_Busy(AxiDma, XAXIDMA_DEVICE_TO_DMA) ) {}
    }

    if (AxiDma->HasMm2S) {
        while ( XAxiDma_Busy(AxiDma, XAXIDMA_DMA_TO_DEVICE) ) {}
    }    
}