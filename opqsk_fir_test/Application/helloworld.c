#include "dma_simple_driver.c"
#include "gpio_driver.c"
#include <xgpio.h>
#include <sleep.h>
#include <xil_printf.h>
#include <xil_types.h>

int main()
{
    /* HEX representation of the coefficients */
    uint32_t hexValues[37] = {
        0x0040959A, 0x00000000, 0xFFAE3170, 0x00000000, 0x006AFA6E, 0x00000000, 0xFF6E1EDE, 0x00000000,
        0x00D2B6F8, 0x00000000, 0xFEB4E07B, 0x00000000, 0x02540589, 0x00000000, 0xFA91486B, 0x00000000,
        0x1B2995E8, 0x40000000, 0x517CC1B7, 0x40000000, 0x1B2995E8, 0x00000000, 0xFA91486B, 0x00000000,
        0x02540589, 0x00000000, 0xFEB4E07B, 0x00000000, 0x00D2B6F8, 0x00000000, 0xFF6E1EDE, 0x00000000,
        0x006AFA6E, 0x00000000, 0xFFAE3170, 0x00000000, 0x0040959A
    };
    init_platform();
    while(1){
        XGpio Gpio0;
        XGpio Gpio1;
        XAxiDma AxiDma_0, AxiDma_1, AxiDma_2;
        XAxiDma_Config * CfgPtr_0, * CfgPtr_1, * CfgPtr_2;

        int Status;

        // xil_printf("\r\n--- Entering main() --- \r\n");
        Status = SetupDma(CfgPtr_0, &AxiDma_0, (UINTPTR) XPAR_XAXIDMA_0_BASEADDR);
        Status = SetupDma(CfgPtr_1, &AxiDma_1, (UINTPTR) XPAR_XAXIDMA_1_BASEADDR);

        
        Status = Setup_Gpio(&Gpio0, ALL_OUTPUTS, CHANNEL1, XPAR_AXI_GPIO_0_BASEADDR);

        TxBufferType_0 *TxBufferPtr_0 = (TxBufferType_0 *) TX_BUFFER_BASE;
        RxBufferType_0 *RxBufferPtr_0 = (RxBufferType_0 *) RX_BUFFER_BASE;

        TxBufferType_1 *TxBufferPtr_1 = (TxBufferType_1 *) TX_BUFFER_BASE;


        TxBufferPtr_0[0] = 0b00001111;
        TxBufferPtr_0[1] = 0b00001111;
        TxBufferPtr_0[2] = 0b00001111;
        TxBufferPtr_0[3] = 0b00001111;
        
        /* Send an input from the DMA to the first peripheral and receive its output if it has any. */ 
        WriteReadDataLoop(&AxiDma_0, (UINTPTR) TxBufferPtr_0, (UINTPTR) RxBufferPtr_0, PERIPHERAL_0_AXIS_PKT_IN, PERIPHERAL_0_AXIS_PKT_OUT);

        /* Print the results through UART and add the line needed for the script to detect the new packet of HEX values */
        xil_printf("------------------------------------------------------------\r\n");
        /* Print the values through UART in HEX form */       
        for (u16 i = 0; i < PERIPHERAL_0_AXIS_PKT_OUT; i+=4) {
            xil_printf("%02x%02x%02x%02x \r\n", RxBufferPtr_0[i + 3], RxBufferPtr_0[i + 2], RxBufferPtr_0[i + 1], RxBufferPtr_0[i]);
        }

        /* Change the coefficients */
        int index = 0;
        for (int i = 0; i < 37; i++) {
            TxBufferPtr_1[index++] = hexValues[i] & 0xFF;
            TxBufferPtr_1[index++] = (hexValues[i] >> 8) & 0xFF;
            TxBufferPtr_1[index++] = (hexValues[i] >> 16) & 0xFF;
            TxBufferPtr_1[index++] = (hexValues[i] >> 24) & 0xFF;
        }

        /* No need for a receive buffer pointer since we only send */        
        WriteReadDataLoop(&AxiDma_1, (UINTPTR) TxBufferPtr_1, (UINTPTR) 0, PERIPHERAL_1_AXIS_PKT_IN, PERIPHERAL_0_AXIS_PKT_OUT);
        TxBufferPtr_0[0] = 0b00001111;
        TxBufferPtr_0[1] = 0b00001111;
        TxBufferPtr_0[2] = 0b00001111;
        TxBufferPtr_0[3] = 0b00001111;
        
        
        /* Send the symbols to the oqpsk */
        WriteReadDataLoop(&AxiDma_0, (UINTPTR) TxBufferPtr_0, (UINTPTR) RxBufferPtr_0, PERIPHERAL_0_AXIS_PKT_IN, PERIPHERAL_0_AXIS_PKT_OUT);
        
        
        /* Signify the new set of hex values */
        xil_printf("------------------------------------------------------------\r\n");
        
        /* Print new set */
        for (u16 i = 0; i < PERIPHERAL_0_AXIS_PKT_OUT; i+=4) {
            xil_printf("%02x%02x%02x%02x \r\n", RxBufferPtr_0[i + 3], RxBufferPtr_0[i + 2], RxBufferPtr_0[i + 1], RxBufferPtr_0[i]);
        }

        /* Signify the end of sets */
        xil_printf("------------------------------------------------------------\r\n");
        
        /* String to stop reading fro mthe serial port, understood by the script */
        xil_printf("final_dma_transfer_julia_close\r\n");
    }

	return XST_SUCCESS;

}