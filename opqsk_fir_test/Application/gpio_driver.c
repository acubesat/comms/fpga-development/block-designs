#include "gpio_driver.h"
#include <xstatus.h>

int Setup_Gpio(XGpio *Gpio, unsigned direction, unsigned channel, UINTPTR base_address)
{
	int Status;

	/* Initialize the GPIO driver iff it is not already initialized */
    if (Gpio->IsReady != XIL_COMPONENT_IS_READY) {
	    Status = XGpio_Initialize(Gpio, base_address);
        if (Status != XST_SUCCESS) {
            xil_printf("Gpio Initialization Failed\r\n");
            return XST_FAILURE;
        }
    }

	/* Set the direction for all signals as inputs except the LED output */
	XGpio_SetDataDirection(Gpio, channel, direction);
    return XST_SUCCESS;
}
