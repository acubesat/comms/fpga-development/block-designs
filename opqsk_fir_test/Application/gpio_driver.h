#include "xparameters.h"
#include "xgpio.h"
#include "xil_printf.h"


#define CHANNEL1 1
#define CHANNEL2 2
#define ALL_OUTPUTS 0x0



#ifdef PRE_2_00A_APPLICATION

#define XGpio_SetDataDirection(InstancePtr, DirectionMask) \
	XGpio_SetDataDirection(InstancePtr, LED_CHANNEL, DirectionMask)

#define XGpio_DiscreteRead(InstancePtr) \
	XGpio_DiscreteRead(InstancePtr, LED_CHANNEL)

#define XGpio_DiscreteWrite(InstancePtr, Mask) \
	XGpio_DiscreteWrite(InstancePtr, LED_CHANNEL, Mask)

#define XGpio_DiscreteSet(InstancePtr, Mask) \
	XGpio_DiscreteSet(InstancePtr, LED_CHANNEL, Mask)

#endif

static int Setup_Gpio(XGpio *Gpio, unsigned direction, unsigned channel, UINTPTR base_address);