#include "dma_simple_driver.c"
#include "dma_simple_driver.h"
#include "gpio_driver.c"
#include <sleep.h>
#include <xil_printf.h>
#include <xil_types.h>
#include "xscugic.h"
#include "xstatus.h"
#include "xil_exception.h"
#include "xttcps.h"
#include "main.h"
#include "xadcps.h"

int main()
{
    xil_printf("\r\n--- Entering main() --- \r\n");
    init_platform();

    XGpio Gpio0, Gpio1, Gpio2, Gpio3;

    XAxiDma AxiDma_0, AxiDma_1;
    XAxiDma_Config * CfgPtr_0, * CfgPtr_1;
    uint8_t Status;

    Status = SetupDma(CfgPtr_0, &AxiDma_0, (UINTPTR) XPAR_XAXIDMA_0_BASEADDR);

    // Status = SetupDma(CfgPtr_1, &AxiDma_1, (UINTPTR) XPAR_XAXIDMA_1_BASEADDR);
    
    
    Status = Setup_Gpio(&Gpio0, ALL_OUTPUTS, CHANNEL1, XPAR_AXI_GPIO_0_BASEADDR);

    Status = Setup_Gpio(&Gpio1, ALL_OUTPUTS, CHANNEL1, XPAR_AXI_GPIO_1_BASEADDR);

    Status = Setup_Gpio(&Gpio2, ALL_OUTPUTS, CHANNEL1, XPAR_AXI_GPIO_2_BASEADDR);
    
    Status = Setup_Gpio(&Gpio3, ALL_OUTPUTS, CHANNEL1, XPAR_AXI_GPIO_3_BASEADDR);

    XGpio_DiscreteWrite(&Gpio0, CHANNEL1, 0x01);
    XGpio_DiscreteWrite(&Gpio1, CHANNEL1, 0x03);
    XGpio_DiscreteWrite(&Gpio2, CHANNEL1, 0x16); 
    XGpio_DiscreteWrite(&Gpio3, CHANNEL1, 0x1);
    
    TxBufferType *DmaDataPtr = (TxBufferType *) TX_BUFFER_BASE;
    uint8_t counter = 0;


    
    while(1){
        WriteReadDataLoop(&AxiDma_0, (UINTPTR) DmaDataPtr, (UINTPTR) 0, PERIPHERAL_0_AXIS_PKT_IN, PERIPHERAL_0_AXIS_PKT_OUT);
        xil_printf("UART functions properly.\r\n");
    }

	return XST_SUCCESS;

}
