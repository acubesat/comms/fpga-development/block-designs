#ifndef GPIO_DRIVER_H
#define GPIO_DRIVER_H

#include "xparameters.h"
#include "xgpio.h"
#include "xil_printf.h"


#define CHANNEL1 1
#define CHANNEL2 2
#define ALL_OUTPUTS 0x0

static uint8_t Setup_Gpio(XGpio *Gpio, unsigned direction, unsigned channel, UINTPTR base_address);

#endif