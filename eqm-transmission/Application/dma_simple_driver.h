#ifndef DMA_SIMPLE_DRIVER_H
#define DMA_SIMPLE_DRIVER_H

#include "xaxidma.h"
#include "xparameters.h"
#include "application_parameters.h"
#include "xdebug.h"
#include "sleep.h"
#include <xil_types.h>


static uint8_t SetupDma(XAxiDma_Config *Config, XAxiDma *AxiDma, 
        UINTPTR BaseAddress);

static void WriteReadDataLoop(XAxiDma *AxiDma, UINTPTR TxBufferPtr, UINTPTR RxBufferPtr, uint32_t PacketsIn, uint32_t PacketsOut);

static void fillBuffer_uint8(uint8_t *buffer, uint8_t *values, size_t numElements);

static void fillBuffer_uint16(uint8_t *buffer, uint16_t *values, size_t numElements);

static void fillBuffer_uint32(uint8_t *buffer, uint32_t *values, size_t numElements);

#define fillBuffer(buffer, values, numElements) _Generic((values), \
    uint8_t*: fillBuffer_uint8, \
    uint16_t*: fillBuffer_uint16, \
    uint32_t*: fillBuffer_uint32 \
)(buffer, values, numElements)

#endif