**Block Designs**

Each block design will have its own folder. In each of these folders there will be the exported hardware `.xsa` file, the `.tcl` script to generate the block design, any additional files that are needed for the block design (e.g. `.xdc` for constraints `.coe` for coefficients etc.), as well as any application files, which cannot be generated by creating a platform in vitis using the provided `.xsa`, in a folder called `Application`. It would also be a good practice to specify the custom IP blocks used in each block design, as well as their version in a README file.
