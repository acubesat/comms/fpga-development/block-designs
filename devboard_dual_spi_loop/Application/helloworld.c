/******************************************************************************
* Copyright (C) 2023 Advanced Micro Devices, Inc. All Rights Reserved.
* SPDX-License-Identifier: MIT
******************************************************************************/
/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include <xstatus.h>
#include "platform.h"
#include "xil_printf.h"
#include "xparameters.h"
#include "xspi.h"
#include "xspi_l.h"

#define BUFFER_SIZE		12
#define SLAVE_SPI_BASEADDR XPAR_XSPI_0_BASEADDR
#define MASTER_SPI_BASEADDR XPAR_XSPI_1_BASEADDR

static XSpi SpiSlaveInstance;
static XSpi SpiMasterInstance;


int main()
{
    init_platform();


    u32 ReadMasterBuffer[BUFFER_SIZE];
    u32 WriteMasterBuffer[BUFFER_SIZE];
    u32 ReadSlaveBuffer[BUFFER_SIZE];
    u32 WriteSlaveBuffer[BUFFER_SIZE];

    print("Hello World\n\r");
    /* Setup slave */
    int StatusSlave;
    XSpi_Config *ConfigPtrSlave;
    ConfigPtrSlave = XSpi_LookupConfig(SLAVE_SPI_BASEADDR);
    if (ConfigPtrSlave == NULL) {
		return XST_FAILURE;
	}

    StatusSlave = XSpi_CfgInitialize(&SpiSlaveInstance, ConfigPtrSlave,
				    ConfigPtrSlave->BaseAddress);
    if (StatusSlave != XST_SUCCESS) {
		return XST_FAILURE;
	}

    /* Options manually set for both slave and master to match (here for slave). These are important options that could cause the two devices to not communicate. They should match for both slave and master. */
    StatusSlave = XSpi_SetOptions(&SpiSlaveInstance, XSP_CLK_PHASE_1_OPTION |
				 XSP_CLK_ACTIVE_LOW_OPTION);
	if (StatusSlave != XST_SUCCESS) {
		return XST_FAILURE;
	}
    XSpi_Start(&SpiSlaveInstance);
    XSpi_IntrGlobalDisable(&SpiSlaveInstance);

    /* Setup master */
    int StatusMaster;
    XSpi_Config *ConfigPtrMaster;
    ConfigPtrMaster = XSpi_LookupConfig(MASTER_SPI_BASEADDR);
    
    if (ConfigPtrMaster == NULL) {
		return XST_DEVICE_NOT_FOUND;
	}

	StatusMaster = XSpi_CfgInitialize(&SpiMasterInstance, ConfigPtrMaster,
				    ConfigPtrMaster->BaseAddress);
	if (StatusMaster != XST_SUCCESS) {
		return XST_FAILURE;
	}

    StatusMaster = XSpi_SelfTest(&SpiMasterInstance);
	if (StatusMaster != XST_SUCCESS) {
		return XST_FAILURE;
	}

    //potential problem due to pointer.
    if (SpiMasterInstance.SpiMode != XSP_STANDARD_MODE) {
		return XST_SUCCESS;
	}

    /* Options manually set for both slave and master to match (here for master). These are important options that could cause the two devices to not communicate. They should match for both slave and master. */
    StatusMaster = XSpi_SetOptions(&SpiMasterInstance, XSP_MASTER_OPTION | XSP_CLK_PHASE_1_OPTION | XSP_CLK_ACTIVE_LOW_OPTION);
	if (StatusMaster != XST_SUCCESS) {
		return XST_FAILURE;
	}

    XSpi_Start(&SpiMasterInstance);
    XSpi_IntrGlobalDisable(&SpiMasterInstance);

    /* Transmission */

    /* Set slave to the first one (and only one) */
    XSpi_SetSlaveSelect(&SpiMasterInstance, 0x01);

    /* Fill the Buffer */
    u8 Test = 0x10;
	for (u8 Count = 0; Count < BUFFER_SIZE; Count++) {
		WriteMasterBuffer[Count] = (u8)(Test + Count);
		ReadMasterBuffer[Count] = 0;
	}

    /* This call transfers the data found in `WriteMasterBuffer` from the master. */ 
    XSpi_Transfer(&SpiMasterInstance, WriteMasterBuffer, ReadMasterBuffer, BUFFER_SIZE);


    u32 slave_SPICR = XSpi_ReadReg(SLAVE_SPI_BASEADDR, XSP_CR_OFFSET);
    u32 master_SPICR = XSpi_ReadReg(MASTER_SPI_BASEADDR, XSP_CR_OFFSET);
    u32 slave_SPISR = XSpi_ReadReg(SLAVE_SPI_BASEADDR, XSP_SR_OFFSET);
    u32 master_SPISR = XSpi_ReadReg(MASTER_SPI_BASEADDR, XSP_SR_OFFSET);
    /* TODO: Look what data type is most appropriate for the pointer based in the data width of the spi word(32 currently) */
    u32 slave_SPIDRR[BUFFER_SIZE];
    
    for (u8 i = 0; i < BUFFER_SIZE; i++) {
        slave_SPIDRR[i] = XSpi_ReadReg(SLAVE_SPI_BASEADDR, XSP_DRR_OFFSET);
    }
    // u32 master_SPDRR = XSpi_ReadReg(XPAR_XSPI_1_BASEADDR, XSP_DRR_OFFSET);
    u32 slave_SPISSR = XSpi_ReadReg(SLAVE_SPI_BASEADDR, XSP_SSR_OFFSET);
    u32 master_SPISSR = XSpi_ReadReg(MASTER_SPI_BASEADDR, XSP_SSR_OFFSET);
    
    cleanup_platform();
    return 0;
}
