This block design is supposed to transmit data in LVDS25 for the external transmitter to read. at86rf215tx previously had a mistake in the AXI-Stream interface and was getting stuck after 8 iterations of the while loop. It no longer gets stuck. Proper data transmission was, for the most part, verified through the integrated logic analyzers.
Project is supposed to work for the pynq-z2 board.
In contrast to the eqm, no led is needed and the gpio code is changed accordingly.
Added debug nets incase they are needed.
**Custom IP Blocks**
- Verilog Oqpsk v4.1
- at86rf215_tx v3.1
