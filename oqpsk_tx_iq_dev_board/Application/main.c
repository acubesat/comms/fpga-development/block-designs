#include "dma_simple_driver.c"
#include "gpio_driver.c"
#include <sleep.h>
#include <xil_printf.h>
#include <xil_types.h>
#include "xscugic.h"
#include "xstatus.h"
#include "xil_exception.h"
// #include "xttcps.h"
#include "main.h"
#include "xadcps.h"

// #define LED_MASK 0x400
#define MARK_SYNC_MASK 0x400
#define SAMPLE_RATE_MASK 0x3c0
#define Q_DATA_0_MASK 0x020
#define SYNC_BITS_MASK 0x01e
#define INVERT_MASK 0x001

int main()
{
    xil_printf("\r\n--- Entering main() --- \r\n");
    // xil_printf("\r\n--- Entering main() --- \r\n");
    /* HEX representation of the coefficients */

    
    // uint32_t status;
    // XScuGic_Config *IntcConfig;
    // XScuGic InterruptController;
    // IntcConfig = XScuGic_LookupConfig(XPAR_INTC_BASEADDR);
    // status = XScuGic_CfgInitialize(&InterruptController, IntcConfig, IntcConfig->CpuBaseAddress);

    init_platform();

    XGpio Gpio0, Gpio1, Gpio2, Gpio3;

    XAxiDma AxiDma_0, AxiDma_1;
    XAxiDma_Config * CfgPtr_0, * CfgPtr_1;

    // static XAdcPs XAdcInst;  // XADC instance
    // XAdcPs_Config *ConfigPtr;
    // XAdcPs_SetSequencerMode(&XAdcInst, XADCPS_SEQ_MODE_SAFE);
    // XAdcPs_SetSeqChEnables(&XAdcInst, XADCPS_SEQ_CH_TEMP);
    // XAdcPs_SetSequencerMode(&XAdcInst, XADCPS_SEQ_MODE_CONTINPASS);
    
    // uint32_t TempRawData;
    // float TempCelsius;

    

    // printf("Temperature: %.2f C\n", TempCelsius);

    uint8_t Status;

    Status = SetupDma(CfgPtr_0, &AxiDma_0, (UINTPTR) XPAR_XAXIDMA_0_BASEADDR);

    // Status = SetupDma(CfgPtr_1, &AxiDma_1, (UINTPTR) XPAR_XAXIDMA_1_BASEADDR);
    
    
    Status = Setup_Gpio(&Gpio0, ALL_OUTPUTS, CHANNEL1, XPAR_AXI_GPIO_0_BASEADDR);

    // Status = Setup_Gpio(&Gpio1, ALL_OUTPUTS, CHANNEL1, XPAR_AXI_GPIO_1_BASEADDR);

    // Status = Setup_Gpio(&Gpio2, ALL_OUTPUTS, CHANNEL1, XPAR_AXI_GPIO_2_BASEADDR);
    
    // Status = Setup_Gpio(&Gpio3, ALL_OUTPUTS, CHANNEL1, XPAR_AXI_GPIO_3_BASEADDR);


    // uint16_t led_value = LED_MASK & (0x001 << 10); 
    uint16_t mark_value = MARK_SYNC_MASK & (0x001 << 10);
    uint16_t sample_rate_value = SAMPLE_RATE_MASK & (0x003 << 6);
    uint16_t q_data_value = Q_DATA_0_MASK & (0x000 << 5);
    uint16_t sync_value = SYNC_BITS_MASK & (0x00a << 1);
    uint16_t invert_value = INVERT_MASK & (0x001 << 0);

    uint16_t gpio_value = mark_value | sample_rate_value | q_data_value | sync_value | invert_value;
    XGpio_DiscreteWrite(&Gpio0, CHANNEL1, gpio_value);
    // XGpio_DiscreteWrite(&Gpio1, CHANNEL1, 0x02);
    // XGpio_DiscreteWrite(&Gpio2, CHANNEL1, 0x09);
    // XGpio_DiscreteWrite(&Gpio3, CHANNEL1, 0x1);
    
    TxBufferType *DmaDataPtr = (TxBufferType *) TX_BUFFER_BASE;
    uint8_t counter = 0;
    while(1){
        xil_printf("%d\r\n", gpio_value);
        
        WriteReadDataLoop(&AxiDma_0, (UINTPTR) DmaDataPtr, (UINTPTR) 0, PERIPHERAL_0_AXIS_PKT_IN, PERIPHERAL_0_AXIS_PKT_OUT);

        xil_printf("aaaaaaaaaaaaaaaa\r\n");
    }

	return XST_SUCCESS;

}