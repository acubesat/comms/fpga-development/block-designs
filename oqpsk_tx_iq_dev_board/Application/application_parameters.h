#ifndef APPLICATION_PARAMETERS
#define APPLICATION_PARAMETERS

#include <stdint.h>
#include "xparameters.h"

#ifdef XPAR_MEM0_BASEADDRESS
#define DDR_BASE_ADDR		XPAR_MEM0_BASEADDRESS
#endif

#ifndef DDR_BASE_ADDR
#define MEM_BASE_ADDR		0x01000000
#else
#define MEM_BASE_ADDR		(DDR_BASE_ADDR + 0x1000000)
#endif

#define TX_BUFFER_BASE		(MEM_BASE_ADDR + 0x00100000)
#define RX_BUFFER_BASE		(MEM_BASE_ADDR + 0x00300000)
#define RX_BUFFER_HIGH		(MEM_BASE_ADDR + 0x004FFFFF)


// #define TEST_START_VALUE	0xC

#define POLL_TIMEOUT_COUNTER    10U

/* Type for the packet received by the DMA */ 
typedef uint8_t RxBufferType;

/* Type for the packet transmitted by the DMA */
typedef uint8_t TxBufferType;


/* General parameters */
#define SYMBOL_BITS         2U
#define SAMPLES_PER_SYMBOL      4U
#define BURST_SIZE          16U

/* Peripheral input and output data width */
#define PERIPHERAL_0_AXIS_SLAVE_DATA_WIDTH    2U
#define PERIPHERAL_0_AXIS_MASTER_DATA_WIDTH   4U
#define PERIPHERAL_1_AXIS_SLAVE_DATA_WIDTH    4U
#define PERIPHERAL_2_AXIS_SLAVE_DATA_WIDTH    1U

/* Number of inputs and outputs expected, in terms of the respective data width */
#define NUM_VALUES_IN_0   (BURST_SIZE)
#define NUM_VALUES_OUT_0  ((SAMPLES_PER_SYMBOL*PERIPHERAL_0_AXIS_SLAVE_DATA_WIDTH*8/SYMBOL_BITS) + (SAMPLES_PER_SYMBOL/SYMBOL_BITS))
#define NUM_VALUES_IN_1 19U
#define NUM_VALUES_IN_2 1U

/* Amount of 8-bit packets to be transmmited */
#define PERIPHERAL_0_AXIS_PKT_IN    (NUM_VALUES_IN_0*PERIPHERAL_0_AXIS_SLAVE_DATA_WIDTH)
#define PERIPHERAL_0_AXIS_PKT_OUT  (NUM_VALUES_OUT_0*PERIPHERAL_0_AXIS_MASTER_DATA_WIDTH)
#define PERIPHERAL_1_AXIS_PKT_IN (NUM_VALUES_IN_1*PERIPHERAL_1_AXIS_SLAVE_DATA_WIDTH)
#define PERIPHERAL_2_AXIS_PKT_IN (NUM_VALUES_IN_2*PERIPHERAL_2_AXIS_SLAVE_DATA_WIDTH)

#endif