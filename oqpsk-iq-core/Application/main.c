#include "dma_simple_driver.c"
#include "gpio_driver.c"
#include <xgpio.h>
#include <sleep.h>

int main()
{
    init_platform();
    XGpio Gpio0;
    XGpio Gpio1;
    XAxiDma AxiDma;
	XAxiDma_Config *CfgPtr;

	int Status;
	TxBufferType *TxBufferPtr = (TxBufferType *) TX_BUFFER_BASE;
	RxBufferType *RxBufferPtr = (RxBufferType *) RX_BUFFER_BASE;

	xil_printf("\r\n--- Entering main() --- \r\n");
    Status = SetupDma(CfgPtr, &AxiDma, (UINTPTR) XPAR_XAXIDMA_0_BASEADDR);
    
    Status = Setup_Gpio(&Gpio0, ALL_OUTPUTS, CHANNEL1, XPAR_AXI_GPIO_0_BASEADDR);
    // Status = XGpio_Initialize(&Gpio0, XPAR_AXI_GPIO_0_BASEADDR);
    // XGpio_SetDataDirection(&Gpio0, CHANNEL1, ALL_OUTPUTS);
    Status = Setup_Gpio(&Gpio1, ALL_OUTPUTS, CHANNEL1, XPAR_AXI_GPIO_1_BASEADDR);
    Status = Setup_Gpio(&Gpio1, ALL_OUTPUTS, CHANNEL2, XPAR_AXI_GPIO_1_BASEADDR);

    XGpio_DiscreteWrite(&Gpio1, CHANNEL1, 0xf);
    XGpio_DiscreteWrite(&Gpio1, CHANNEL1, 0x1);



    for(int i = 0; i < 8; i++) {
        TxBufferPtr[i] = (0b10100101);
    }

    // WriteReadDataLoop(&AxiDma, TxBufferPtr, RxBufferPtr);
    volatile int delay;
    while(1){
        XGpio_DiscreteWrite(&Gpio0, CHANNEL1, 0x01);
        for (delay = 0; delay < 10000000; delay++);
        XGpio_DiscreteClear(&Gpio0, CHANNEL1, 0x01);
        for (delay = 0; delay < 10000000; delay++);
    }

	return XST_SUCCESS;

}