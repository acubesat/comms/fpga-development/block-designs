#include "dma_simple_driver.h"
#include <xil_types.h>
#include <xstatus.h>

static int CheckData(RxBufferType *RxPacket, TxBufferType *TxPacket)
{
	Xil_DCacheInvalidateRange((UINTPTR)RxPacket, OQPSK_PKT_LEN_OUT);


	for (int Index = 0; Index < NUM_OF_VARS_OQPSK_OUT; Index++) {
        xil_printf("%x\r\n", RxPacket[Index]);
	}

	return XST_SUCCESS;
}

static int SetupDma(XAxiDma_Config *CfgPtr, XAxiDma *AxiDma, UINTPTR BaseAddress)
{
    int Status;
	CfgPtr = XAxiDma_LookupConfig(BaseAddress);
	if (!CfgPtr) {
		xil_printf("No config found for %d\r\n", BaseAddress);
		return XST_FAILURE;
	}

	Status = XAxiDma_CfgInitialize(AxiDma, CfgPtr);
	if (Status != XST_SUCCESS) {
		xil_printf("Initialization failed %d\r\n", Status);
		return XST_FAILURE;
	}

	if (XAxiDma_HasSg(AxiDma)) {
		xil_printf("Device configured as SG mode \r\n");
		return XST_FAILURE;
	}

	/* Disable interrupts, we use polling mode
	 */
	XAxiDma_IntrDisable(AxiDma, XAXIDMA_IRQ_ALL_MASK,
			    XAXIDMA_DEVICE_TO_DMA);
	XAxiDma_IntrDisable(AxiDma, XAXIDMA_IRQ_ALL_MASK,
			    XAXIDMA_DMA_TO_DEVICE);

    return XST_SUCCESS;
}

static void WriteReadDataLoop(XAxiDma *AxiDma, TxBufferType *TxBufferPtr, RxBufferType *RxBufferPtr)
{
	int Status;
	int TimeOut = POLL_TIMEOUT_COUNTER;

	Xil_DCacheFlushRange( (UINTPTR)TxBufferPtr, OQPSK_PKT_LEN_IN);
	Xil_DCacheFlushRange( (UINTPTR)RxBufferPtr, OQPSK_PKT_LEN_OUT);





    Status = XAxiDma_SimpleTransfer(AxiDma, (UINTPTR)TxBufferPtr,
            OQPSK_PKT_LEN_IN, XAXIDMA_DMA_TO_DEVICE);

    if (Status != XST_SUCCESS) {
        xil_printf("Simple transfer from DMA to device failed");
        return;
    }

    Status = XAxiDma_SimpleTransfer(AxiDma, (UINTPTR)RxBufferPtr,
            OQPSK_PKT_LEN_OUT, XAXIDMA_DEVICE_TO_DMA);

    if (Status != XST_SUCCESS) {
        xil_printf("Simple transfer from device to DMA failed");
        return;
    }
    
      

    while (TimeOut) {
        if (!(XAxiDma_Busy(AxiDma, XAXIDMA_DEVICE_TO_DMA)) &&
            !(XAxiDma_Busy(AxiDma, XAXIDMA_DMA_TO_DEVICE))) {
            break;
        }
        TimeOut--;
        usleep(1U);
    }       
}