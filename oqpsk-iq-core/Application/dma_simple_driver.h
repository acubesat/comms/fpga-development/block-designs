#include "xaxidma.h"
#include "xparameters.h"
#include "xdebug.h"
#include "sleep.h"


#if defined(XPAR_UARTNS550_0_BASEADDR)
#include "xuartns550_l.h"       /* to use uartns550 */
#endif

/******************** Constant Definitions **********************************/

/*
 * Device hardware build related constants.
 */


#ifdef XPAR_MEM0_BASEADDRESS
#define DDR_BASE_ADDR		XPAR_MEM0_BASEADDRESS
#endif

#ifndef DDR_BASE_ADDR
#define MEM_BASE_ADDR		0x01000000
#else
#define MEM_BASE_ADDR		(DDR_BASE_ADDR + 0x1000000)
#endif

#define TX_BUFFER_BASE		(MEM_BASE_ADDR + 0x00100000)
#define RX_BUFFER_BASE		(MEM_BASE_ADDR + 0x00300000)
#define RX_BUFFER_HIGH		(MEM_BASE_ADDR + 0x004FFFFF)


// #define TEST_START_VALUE	0xC

#define POLL_TIMEOUT_COUNTER    1000000U

/* Type for the packet received by the DMA */ 
typedef u32 RxBufferType;

/* Type for the packet transmitted by the DMA */
typedef u8 TxBufferType;

#define SYMBOL_BITS         2U
#define SAMPLES_PER_SYMBOL      4U
#define NUM_OF_SYMBOLS      32U
#define OQPSK_AXIS_BYTES_IN       8U
#define OQPSK_AXIS_BYTES_OUT      4U
#define NUM_OF_VARS_OQPSK_OUT       NUM_OF_SYMBOLS*SAMPLES_PER_SYMBOL
#define OQPSK_PKT_LEN_IN		OQPSK_AXIS_BYTES_IN/sizeof(TxBufferType)
#define OQPSK_PKT_LEN_OUT		SAMPLES_PER_SYMBOL*OQPSK_AXIS_BYTES_OUT*NUM_OF_SYMBOLS/sizeof(RxBufferType)


#if (!defined(DEBUG))
extern void xil_printf(const char *format, ...);
#endif

static int CheckData(RxBufferType *RxPacket, TxBufferType *TxPacket);
static int SetupDma(XAxiDma_Config *Config, XAxiDma *AxiDma, 
        UINTPTR BaseAddress);
static void WriteReadDataLoop(XAxiDma *AxiDma, TxBufferType *TxBufferPtr, RxBufferType *RxBufferPtr);