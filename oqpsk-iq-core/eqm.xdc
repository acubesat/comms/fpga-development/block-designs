set_property -dict { PACKAGE_PIN N17   IOSTANDARD LVCMOS33 } [get_ports { led }];
set_property -dict { PACKAGE_PIN P14   IOSTANDARD LVCMOS33 } [get_ports { underflow_0 }]; 
set_property -dict { PACKAGE_PIN B20   IOSTANDARD LVDS_25 } [get_ports { data_out_to_pins_n_0 }];
set_property -dict { PACKAGE_PIN C20   IOSTANDARD LVDS_25 } [get_ports { data_out_to_pins_p_0 }];
set_property -dict { PACKAGE_PIN L17   IOSTANDARD LVDS_25  } [get_ports { clk_to_pins_n_0 }];
set_property -dict { PACKAGE_PIN L16   IOSTANDARD LVDS_25  } [get_ports { clk_to_pins_p_0 }];